const workloadHandler = () => {
    const workloadToClean = document.getElementById('workloadToClean');

    async function cleanLayout(codeToLayout){
	    res = await fetch('https://www.10bestdesign.com/dirtymarkup/api/html', {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
	            code: codeToLayout,
	            output: 'fragment'
            })
        });
        console.log(res);
        let {clean} = await res.json();
        console.log(clean);
        return clean;
    }

    const clean = (sourceInput) => {
        console.log(`Received ${sourceInput}`);

        let output = sourceInput
            .replace(/style=".*?"/g,'')
            .replace(/&nbsp;/g,' ')
            .replace(/<\/?span>/g, '')
            .replace(/<\/strong><strong>/g, '')
            .replace(/data-gtm-vis-has-fired-[0-9_="]*/g, '')
            .replace(/<\/?em.*?>/g, '')
            .replace(/<br.*?>/g, '')
            .replace(/\s>/g, '>')
            .replace(/<\/?span>/g, '')
            .replace(/<\/?html>|<\/?body>|<!--\w+-->/g, '')
            .replace(/<\/?mark.*?>/g, '')
            .replace(/<meta.*?>/g, '')
            .replace(/(<\/(p|li|h[34])>)/g,'$1\n');

        console.log(`Produced ${output}`);

        //output = cleanLayout(output);
        //console.log(`Formatted ${output}`);

        navigator.clipboard.writeText(output)
        .then(() => {console.log('Added to clipboard');})
        .catch(err => {
            console.log('Something went wrong', err);
        });

        return output;
    };

    workloadToClean.addEventListener('input', (e) => {
        const elem = e.target;
        elem.value = clean(elem.value);
    });

    workloadToClean.addEventListener('paste' , (e) => {
	    e.preventDefault();
	    let data = e.clipboardData.getData('text/html') || e.clipboardData.getData('text/plain');
	    console.log(data);
        workloadToClean.value = clean(data);
    })
};

window.addEventListener('load', workloadHandler);