const loadHandler = () => {
    // Elements
    const textToSentenceCaseElem = document.getElementById('textToSentenceCase');
    const textToCleanElem = document.getElementById('textToClean');
    const emailElem = document.getElementById('email');
    const phoneElem = document.getElementById('phone');
    const spaceElem = document.getElementById('spaceOut');

    const imgSElem = document.getElementById('img640');
    const imgMElem = document.getElementById('img1000');
    const imgLElem = document.getElementById('img1200');

    const erIn = document.getElementById('erIn');
    const erLang = document.getElementById('erLang');
    const erOut = document.getElementById('erOut');

    console.log(imgSElem, imgMElem, imgLElem);

    // Text manipulation functions
    imgSizes = (url, newDimension) => url.replace(/\d+x\d+/, newDimension); 

    const sentenceCase = (source) => source.split(' ').map(
		(word) => {
			let first = word.slice(0,1),
				rest = word.slice(1);
			return `${first.toUpperCase()}${rest.toLowerCase()}`;
		}
	).join(' ');

    // Convert email to mailto link
    const markUpEmail = (email) => `<a href="mailto:${email}">${email}</a>`;

    const formatPhone = (phone) => {
        phone = phone.replace(/[\(\)\+]/g, '');
        let parts = phone.split(' ');
        let start = `(+${parts[0]}) `;
        return start + parts.slice(1).join('-');
    };

    // Remove nbsp
    // Use break instead of paragraph tags
    // Clean and sentence case titles
    const strip = (source) => source.replace(/&nbsp;/g, ' ')
                                .replace(/,/g, '</br>')
                                .replace(/<p.*?>/g, '')
                                .replace(/<\/p>/g, '<br/>')
                                .replace(/<\/?span.*?>/g, '')
                                .replace(/<h4><a href="(.*?)">(.*)<\/a><\/h4>/, (match, p1, p2) => `<h4>${p2}</h4><a href="${p1}">${p1}</a>`)
                                .replace(/<h4.*?>(.*)<\/h4>/g, (match, p1) => `<h4>${sentenceCase(p1)}</h4>`)
                                .replace(/Tel: (\+?[0-9-\(\)\ ]*)/g, (match, p1) => `</br>Tel: ${formatPhone(p1)}`)
                                .replace(/Mobile: (\+?[0-9-\(\)\ ]*)/g, (match, p1) => `</br>Mobile: ${formatPhone(p1)}</br>`);

    const spaceOut = (text) => text.replace(/-/g, ' ');

    const erPurge = (source) => source.replace(/&nbsp;/g, ' ')
                                .replace(/<p.*?>/g, '')
								.replace(/<\/p>/g, '<br/>')
								.replace(/<br.*?>/g, '<br />')
								.replace(/<p>\s+<\/p>/g, '')
								.replace(/&amp;/g, '&')
								.replace(/style=".*?"/, '')

    const prepareEntryRequirements = () => {
        let srcIn = erIn.value;
        let srcLang = erLang.value;
        console.log(srcIn, srcLang);

        srcIn = `<ul>${erPurge(srcIn)}</ul>`;
        srcLang = `<ul>${erPurge(srcLang)}</ul>`;

        erOut.value = `${srcIn} ${srcLang}`
    }


    // Event handlers
    textToSentenceCaseElem.addEventListener('input', (e) => {
        const elem = e.target;
        elem.value = sentenceCase(elem.value);
    });

    textToCleanElem.addEventListener('input', (e) => {
        const elem = e.target;
        elem.value = strip(elem.value);
    });

    emailElem.addEventListener('input', (e) => {
        const elem = e.target;
        elem.value = markUpEmail(elem.value);
    });

    phoneElem.addEventListener('input', (e) => {
        const elem = e.target;
        elem.value = formatPhone(elem.value);
    });

    spaceElem.addEventListener('input', (e) => {
        const elem = e.target;
        elem.value = spaceOut(elem.value);
    });

    let calculateDimensions = (url) => {
        imgSElem.value = imgSizes(url, '640x400');
        imgMElem.value = imgSizes(url, '1000x600');
        imgLElem.value = imgSizes(url, '1200x400');
    }

    // Should attach this to a parent element
    imgSElem.addEventListener('input', (e) => {
        const url = e.target.value;
        console.log(url);
        calculateDimensions(url);
    });

    imgMElem.addEventListener('input', (e) => {
        const url = e.target.value;
        console.log(url);
        calculateDimensions(url);
    });

    imgLElem.addEventListener('input', (e) => {
        const url = e.target.value;
        console.log(url);
        calculateDimensions(url);
    });

    erIn.addEventListener('input', (e) => {
        const source = e.target.value;
        console.log(source);
        prepareEntryRequirements();
    });

    erLang.addEventListener('input', (e) => {
        const source = e.target.value;
        console.log(source);
        prepareEntryRequirements();
    });


};

window.addEventListener('load', loadHandler);